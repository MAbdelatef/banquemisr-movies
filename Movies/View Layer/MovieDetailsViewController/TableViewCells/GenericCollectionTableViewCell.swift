
//
//  GenericCollectionTableViewCell.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxSwift

class GenericCollectionTableViewCell: BaseTableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - variables
    private var viewModel:GenericCollectionCellViewModel!
    override var baseViewModel: BaseCellViewModel!{
        didSet{
            viewModel = baseViewModel as? GenericCollectionCellViewModel
            bindUI()
            viewModel.configureData()
        }
    }
    
    // MARK: - UI Configuration
    override func configureUI() {
        super.configureUI()
        configureCollectionView()
    }
    
    func configureCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 8
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    // MARK: - binding
    override func bindUI(){
        super.bindUI()
        viewModel.reloadSubject.observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] _ in
            guard let self = self else { return }
            self.registerCells()
            self.collectionView.reloadData()
        }).disposed(by: bag)
    }
    
    // MARK: - Helpers
    func registerCells(){
        let reuseIdentifiers = viewModel.getCellReuseIdentifiers()
        reuseIdentifiers.forEach { (identifer) in
            collectionView.register(UINib(nibName: identifer, bundle: nil), forCellWithReuseIdentifier: identifer)
        }
    }
}

extension GenericCollectionTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellViewModel = viewModel.dataSource[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellViewModel.cellIdentifier, for: indexPath) as! BaseCollectionViewCell
        cell.baseViewModel = cellViewModel
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.getItemSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.handleSelectionFor(index: indexPath.row)
    }
}
