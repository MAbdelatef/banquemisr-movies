//
//  MovieDetailsHeaderTableViewCell.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage
import RxSwift
import RxCocoa


class MovieDetailsHeaderTableViewCell: BaseTableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var favoriteButton: UILoadingButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var genreCollectionView: UICollectionView!
    
    // MARK: - variables
    private var viewModel:MovieDetailsHeaderCellViewModel!
    override var baseViewModel: BaseCellViewModel!{
        didSet{
            self.viewModel = baseViewModel as? MovieDetailsHeaderCellViewModel
            bindUI()
        }
    }
    
    // MARK: - UI Configuration
    override func configureUI() {
        super.configureUI()
        configureCollectionView()
    }
    
    func configureCollectionView(){
        let layout = LeftAlignedCollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 50, height: 30)
        layout.minimumInteritemSpacing = 5
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        genreCollectionView.collectionViewLayout = layout
        genreCollectionView.isScrollEnabled = true
    }
    
    // MARK: - binding
    override func bindUI(){
        super.bindUI()
        if let posterURL = viewModel.moviePosterURL{
            let url = URL(string: posterURL)
            movieImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "icn_tmdb_rectangle"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        movieTitleLabel.text = viewModel.movieTitle
        yearLabel.text = viewModel.year
        ratingView.rating = Double(viewModel.stars)
        ratingView.text = viewModel.rateText
        
        viewModel.showLoadingSubject.asObservable().subscribe(onNext: { [weak self] (loadingType) in
            guard let self = self else { return }
            if loadingType == .Custom{
                self.favoriteButton.showLoading(color: lightGreen!)
            }
        }).disposed(by: bag)
        
        viewModel.hideLoadingSubject.asObservable().subscribe(onNext: { [weak self] (loadingType) in
            guard let self = self else { return }
            if loadingType == .Custom{
                self.favoriteButton.hideLoading()
            }
        }).disposed(by: bag)
        
        viewModel.isFavorite.asObservable().subscribe(onNext: { [weak self] (isFavorite) in
            guard let self = self else { return }
            let imageName = isFavorite ? "icn_FavoriteFilled" : "icn_Favorite"
            self.favoriteButton.setImage(UIImage(named: imageName), for: .normal)
        }).disposed(by: bag)
        
        configureCollectionViewBinding()
    }
    
    func configureCollectionViewBinding(){
        genreCollectionView.register(UINib(nibName: GenreCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: GenreCollectionViewCell.identifier)
        
        viewModel.genreDataSource
            .observeOn(MainScheduler.instance)
            .bind(to: genreCollectionView.rx
                .items(cellIdentifier: GenreCollectionViewCell.identifier,cellType: GenreCollectionViewCell.self)) {
                    _, viewModel, cell in
                    cell.baseViewModel = viewModel
        }.disposed(by: bag)
    }
    
    // MARK: - actions
    @IBAction func favoriteButtonAction(_ sender: Any) {
        viewModel.handleFavoriteAction()
    }
}
