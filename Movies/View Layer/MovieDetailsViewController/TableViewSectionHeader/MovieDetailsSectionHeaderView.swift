//
//  MovieDetailsSectionHeaderView.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class MovieDetailsSectionHeaderView: BaseTableHeaderFooterView {
    
    // MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var colorView: UIView!

    // MARK: - variables
    private var viewModel:MovieDetailsHeaderViewModel!
    override var baseViewModel: BaseHeaderViewModel!{
        didSet{
            self.viewModel = baseViewModel as? MovieDetailsHeaderViewModel
            bindUI()
        }
    }
    
    // MARK: - view life cycle
    override func prepareForReuse() {
        super.prepareForReuse()
        button.isHidden = true
        headerTitleLabel.text = nil
    }
    
    // MARK: - Actions
    override func bindUI() {
        headerTitleLabel.text = viewModel.title
        if let buttonTitle = viewModel.buttonTitle {
            button.setTitle(buttonTitle, for: .normal)
            button.isHidden = false
        }
        else{
            button.isHidden = true
        }
    }
    
    // MARK: - Actions
    @IBAction func buttonAction(_ sender: Any) {
        viewModel.handleButtonAction()
    }
    
    
}
