//
//  GenreCollectionViewCell.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class GenreCollectionViewCell: BaseCollectionViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - variables
     private var viewModel:GenreCellViewModel!
     override var baseViewModel: BaseCellViewModel!{
         didSet{
             self.viewModel = baseViewModel as? GenreCellViewModel
             bindUI()
         }
     }
    
    // MARK: - UI Configuration
    override func configureUI() {
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = lightGreen!.cgColor
    }
    
    // MARK: - binding
    override func bindUI(){
        titleLabel.text = viewModel.genretTitle
    }

}
