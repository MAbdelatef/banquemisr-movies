//
//  ActorCollectionViewCell.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import SDWebImage

class GenericDetailsCollectionViewCell: BaseCollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    // MARK: - variables
    private var viewModel:GenericDetailsCollectionCellViewModel!
    override var baseViewModel: BaseCellViewModel!{
        didSet{
            self.viewModel = baseViewModel as? GenericDetailsCollectionCellViewModel
            bindUI()
        }
    }
    
    // MARK: - view life cycle
    override func prepareForReuse() {
        super.prepareForReuse()
        contentImageView.image = nil
    }
    
    // MARK: - UI Configuration
    override func configureUI() {
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = lightGreen!.cgColor
    }
    
    // MARK: - binding
    override func bindUI(){
        if let imageURL = viewModel.imageURL{
            let url = URL(string: imageURL)
            contentImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "icn_tmdb_rectangle"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        else{
            contentImageView.image = UIImage(named: "icn_tmdb_rectangle")
        }
        titleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
    }
}
