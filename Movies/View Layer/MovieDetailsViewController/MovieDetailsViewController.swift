//
//  MovieDetailsViewController.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxSwift

class MovieDetailsViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - variables
    private var viewModel:MovieDetailsViewModel!
    override var baseViewModel: BaseViewModel!{
        didSet{
            self.viewModel = baseViewModel as? MovieDetailsViewModel
        }
    }
    
    // MARK: - initialization
    init(viewModel:MovieDetailsViewModel) {
        super.init(nibName: MovieDetailsViewController.identifier, bundle: nil)
        baseViewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - view lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureTableViewUI()
        configureBindings()
        bindNavigationBarTitle()
        viewModel.loadData()
    }
    
    // MARK: - UI Configuration
    func configureTableViewUI(){
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.separatorStyle = .none
        tableView.backgroundColor = dark!
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func registerCellsAndHeaders(){
        let cellReuseIdentifiers = viewModel.getCellReuseIdentifiers()
        cellReuseIdentifiers.forEach { (identifer) in
            tableView.register(UINib(nibName: identifer, bundle: nil), forCellReuseIdentifier: identifer)
        }
        
        let headersReuseIdentifiers = viewModel.getHeaderReuseIdentifiers()
        headersReuseIdentifiers.forEach { (identifer) in
            tableView.register(UINib(nibName: identifer, bundle: nil), forHeaderFooterViewReuseIdentifier: identifer)
        }
    }
    
    func configureNavigationBar(){
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "icn_close"), style: .plain, target: self, action: #selector(handleCloseButtonAction))
        self.navigationItem.leftBarButtonItem = barButtonItem
        self.navigationItem.title = viewModel.viewTitle
    }
    
    @objc func handleCloseButtonAction(){
        viewModel.dismissView()
    }
    
    // MARK: - binding
    override func configureBindings(){
        super.configureBindings()
        viewModel.reloadSubject.observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (_) in
                guard let self = self else { return }
                self.registerCellsAndHeaders()
                self.tableView.reloadData()
            }).disposed(by: bag)
        
        viewModel.showMovieDetails.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (detailsViewModel) in
                guard let self = self else { return }
                let detailsViewController = MovieDetailsViewController(viewModel: detailsViewModel)
                let navigationController = UINavigationController(rootViewController: detailsViewController)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true, completion: nil)
            }).disposed(by: bag)
        
        viewModel.showMoviesListing.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (listingViewModel) in
                guard let self = self else { return }
                let movieListingViewController = MovieListingViewController(viewModel: listingViewModel)
                self.navigationController?.pushViewController(movieListingViewController, animated: true)
            }).disposed(by: bag)
    }
    
    func bindNavigationBarTitle(){
        navigationItem.title = viewModel.viewTitle
    }
}

// MARK: - table dataSource & delegate methods
extension MovieDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionViewModel = viewModel.dataSource[section]
        return sectionViewModel.cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionViewModel = viewModel.dataSource[indexPath.section]
        let cellViewModel = sectionViewModel.cellViewModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellViewModel.cellIdentifier, for: indexPath) as! BaseTableViewCell
        cell.baseViewModel = cellViewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionViewModel = viewModel.dataSource[section]
        guard sectionViewModel.headerEnabled else { return nil }
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionViewModel.headerIdentifier) as! BaseTableHeaderFooterView
        headerView.baseViewModel = sectionViewModel
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionViewModel = viewModel.dataSource[section]
        return sectionViewModel.headerHeight()
    }
    
}
