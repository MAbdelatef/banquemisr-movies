//
//  MovieTableViewCell.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class MovieTableViewCell: BaseTableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - variables
    private var viewModel:MovieCellViewModel!
    override var baseViewModel: BaseCellViewModel!{
        didSet{
            self.viewModel = baseViewModel as? MovieCellViewModel
            bindUI()
        }
    }
    
    // MARK: - UI Configuration
    override func configureUI() {
        super.configureUI()
        containerView.layer.cornerRadius = 8
        movieImageView.layer.cornerRadius = 8
    }
    
    // MARK: - binding
    override func bindUI(){
        super.bindUI()
        if let posterURL = viewModel.moviePosterURL{
            let url = URL(string: posterURL)
            movieImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "icn_tmdb_rectangle"), options: SDWebImageOptions.highPriority, completed: nil)
        }
        movieTitleLabel.text = viewModel.movieTitle
        releaseDateLabel.text = viewModel.releaseDate
        ratingView.rating = Double(viewModel.stars)
        ratingView.text = viewModel.rateText
    }
}
