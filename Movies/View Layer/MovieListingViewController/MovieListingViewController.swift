//
//  MovieListingViewController.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/6/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxSwift
import UIScrollView_InfiniteScroll

class MovieListingViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    private let refreshControl:UIRefreshControl = UIRefreshControl()
    
    // MARK: - variables
    private var viewModel:MovieListingViewModel!
    override var baseViewModel: BaseViewModel!{
        didSet{
            self.viewModel = baseViewModel as? MovieListingViewModel
        }
    }
    
    // MARK: - initialization
    init(viewModel:MovieListingViewModel) {
        super.init(nibName: MovieListingViewController.identifier, bundle: nil)
        baseViewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - view lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableViewUI()
        configureRefreshControl()
        configureBindings()
        bindNavigationBarTitle()
        configureTableViewBinding()
        configureTableViewSelection()
        viewModel.loadIntialData()
    }
    
    // MARK: - UI Configuration
    func configureTableViewUI(){
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.separatorStyle = .none
        tableView.backgroundColor = dark!
        
        tableView.infiniteScrollIndicatorView = CustomInfiniteScrollIndicatorView()
        tableView.setShouldShowInfiniteScrollHandler { [weak self] (_) -> Bool in
            guard let self = self else { return false}
            return self.viewModel.loadMoreEnabled
        }
        tableView.addInfiniteScroll { [weak self] (_) in
            guard let self = self else { return }
            self.viewModel.loadMore()
        }
    }
    
    func configureRefreshControl() {
        refreshControl.tintColor = lightGreen!
        refreshControl.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    // MARK: - binding
    override func configureBindings(){
        super.configureBindings()
        viewModel.hideLoadingSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (loadingType) in
                guard let self = self else { return }
                if loadingType == .PullToRefresh {
                    self.refreshControl.endRefreshing()
                }
                else if loadingType == .LoadMore {
                    self.tableView.finishInfiniteScroll(completion: nil)
                }
            })
            .disposed(by: bag)
        
        viewModel.showMovieDetails.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (detailsViewModel) in
                guard let self = self else { return }
                let detailsViewController = MovieDetailsViewController(viewModel: detailsViewModel)
                let navigationController = UINavigationController(rootViewController: detailsViewController)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true, completion: nil)
            }).disposed(by: bag)
    }
    
    func bindNavigationBarTitle(){
        navigationItem.title = viewModel.viewTitle
    }
    
    func configureTableViewBinding(){
        tableView.register(UINib(nibName: MovieTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: MovieTableViewCell.identifier)
        
        viewModel.dataSource
            .observeOn(MainScheduler.instance)
            .bind(to: tableView
                .rx
                .items(cellIdentifier: MovieTableViewCell.identifier, cellType: MovieTableViewCell.self)) {
                    rowIndex, cellViewModel, cell in
                    cell.baseViewModel = cellViewModel
        }
        .disposed(by: bag)
    }
    
    func configureTableViewSelection() {
        tableView
            .rx
            .modelSelected(MovieCellViewModel.self).observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] cellViewModel in
                self.viewModel.handleSelectionFor(cellViewModel: cellViewModel)
            })
            .disposed(by: bag)
    }
    
    // MARK: - actions
    @objc func handleRefreshControl(){
        viewModel.handlePullToRefresh()
    }
}
