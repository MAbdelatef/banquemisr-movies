//
//  BaseCollectionViewCell.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {

    var baseViewModel: BaseCellViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    func configureUI(){
        
    }
    
    func bindUI(){
        
    }
    
}
