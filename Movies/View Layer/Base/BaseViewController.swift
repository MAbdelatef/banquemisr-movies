//
//  BaseViewController.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxSwift
import NVActivityIndicatorView

class BaseViewController: UIViewController {
    
    // MARK: - variables
    var baseViewModel:BaseViewModel!
    let bag = DisposeBag()
    var emptyStateView:EmptyStateView?
    
    // MARK: - bindings
    func configureBindings() {
        baseViewModel.showLoadingSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { (loadingType) in
                if loadingType == .Default {
                    let activityData = ActivityData(color: lightGreen!)
                    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData,nil)
                }
            })
            .disposed(by: bag)
        
        baseViewModel.hideLoadingSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: {(loadingType) in
                if loadingType == .Default {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                }
            })
            .disposed(by: bag)
        
        baseViewModel.showEmptyStateSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (emptyStateViewModel) in
                guard let self  = self else { return }
                //Remove Old EmptyState
                if let view = self.emptyStateView {
                    view.removeFromSuperview()
                }
                
                self.emptyStateView = EmptyStateView.emptyStateView(viewModel: emptyStateViewModel, frame: self.view.bounds)
                self.view.addSubview(self.emptyStateView!)
                self.emptyStateView?.fillSuperview()
            })
            .disposed(by: bag)
        
        baseViewModel.showAlertSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (alertModel) in
                guard let self  = self else { return }
                let alertController = UIAlertController(title: alertModel.title, message: alertModel.message, preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: alertModel.cancelButtonTitle, style: .cancel, handler: nil)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            })
            .disposed(by: bag)
        
        
        baseViewModel.hideEmptyStateSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self  = self else { return }
                self.emptyStateView?.removeFromSuperview()
                self.emptyStateView = nil
            })
            .disposed(by: bag)
        
        baseViewModel.dismissViewControllerSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self  = self else { return }
                self.dismiss(animated: true, completion: nil)
            })
            .disposed(by: bag)
        
        baseViewModel.popViewControllerSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self  = self else { return }
                self.navigationController?.popViewController(animated: true)
            })
            .disposed(by: bag)
    }
    
    deinit {
        print(String(describing: self) + "deinit")
    }
}
