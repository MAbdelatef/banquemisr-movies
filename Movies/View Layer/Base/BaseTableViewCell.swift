//
//  BaseTableViewCell.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxSwift
import NVActivityIndicatorView

class BaseTableViewCell: UITableViewCell {
    
    // MARK: - variables
    var baseViewModel: BaseCellViewModel!
    var bag = DisposeBag()
    var emptyStateView:EmptyStateView?
    
    // MARK: - view Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        configureUI()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        bag = DisposeBag()
    }
    
    // MARK: - UI Configuration
    func configureUI(){
        
    }
    
    // MARK: - bindings
    func bindUI(){
        baseViewModel.showLoadingSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { (loadingType) in
                if loadingType == .Default {
                    let activityData = ActivityData(color: lightGreen! )
                    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData,nil)
                }
            })
            .disposed(by: bag)
        
        baseViewModel.hideLoadingSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: {(loadingType) in
                if loadingType == .Default {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                }
            })
            .disposed(by: bag)
        
        baseViewModel.showEmptyStateSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (emptyStateViewModel) in
                guard let self  = self else { return }
                
                //Remove Old EmptyState
                if let view = self.emptyStateView {
                    view.removeFromSuperview()
                }
                
                self.emptyStateView = EmptyStateView.emptyStateView(viewModel: emptyStateViewModel, frame: self.bounds)
                self.addSubview(self.emptyStateView!)
                self.emptyStateView?.fillSuperview()
            })
            .disposed(by: bag)
        
        
        baseViewModel.hideEmptyStateSubject.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self  = self else { return }
                self.emptyStateView?.removeFromSuperview()
                self.emptyStateView = nil
            })
            .disposed(by: bag)
    }
    
    deinit {
        print(String(describing: self) + "deinit")
    }
}
