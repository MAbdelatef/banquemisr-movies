//
//  LoginViewController.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxBiBinding

class LoginViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet var conatinerViews: [UIView]!
    
    // MARK: - variables
    private var viewModel:LoginViewModel!
    override var baseViewModel: BaseViewModel!{
        didSet{
            self.viewModel = baseViewModel as? LoginViewModel
        }
    }
    
    // MARK: - intialization
    init(viewModel:LoginViewModel) {
        super.init(nibName: LoginViewController.identifier, bundle: nil)
        baseViewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureBindings()
    }
    
    func configureUI(){
        conatinerViews.forEach { (view) in
            view.layer.cornerRadius = 10
        }
        let placeholderAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Username", attributes: placeholderAttributes)
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: placeholderAttributes)
        
        loginButton.layer.borderWidth = 1.0
        loginButton.layer.borderColor = lightGreen!.cgColor
        loginButton.setTitleColor(lightGreen, for: .normal)
        loginButton.setTitle("Login", for: .normal)
    }
    
    override func configureBindings() {
        super.configureBindings()
        (usernameTextField.rx.text <-> viewModel.username).disposed(by: bag)
        (passwordTextField.rx.text <-> viewModel.password).disposed(by: bag)
        
        viewModel.showTabBar.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                let moviesTabBarController = MoviesTabBarViewController()
                moviesTabBarController.modalPresentationStyle = .fullScreen
                self.present(moviesTabBarController, animated: true, completion: nil)
            }).disposed(by: bag)
    }
    
    // MARK: - actions
    @IBAction func loginButtonAction(_ sender: Any) {
        viewModel.handleLoginAction()
    }
}
