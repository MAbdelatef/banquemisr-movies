//
//  MoviesTabBarViewController.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class MoviesTabBarViewController: UITabBarController {
    
    private var viewModel:MoviesTabBarViewModel = MoviesTabBarViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewControllers()
        configureAppearance()
    }
    
    func configureViewControllers(){
        
        let homeViewController = MovieListingViewController(viewModel: viewModel.homeViewModel)
        if let homeTabBarItemViewModel = viewModel.homeViewModel.tabBarItemViewModel{
            homeViewController.tabBarItem = UITabBarItem(title: homeTabBarItemViewModel.title, image: UIImage(named: homeTabBarItemViewModel.iconName), selectedImage: nil)
        }
        let homeNavigationController = UINavigationController(rootViewController: homeViewController)
        
        let favoritesViewController = MovieListingViewController(viewModel: viewModel.favoritesViewModel)
        if let favoritesTabBarItemViewModel = viewModel.favoritesViewModel.tabBarItemViewModel{
            favoritesViewController.tabBarItem = UITabBarItem(title: favoritesTabBarItemViewModel.title, image: UIImage(named: favoritesTabBarItemViewModel.iconName), selectedImage: nil)
        }
        let favoritesNavigationController = UINavigationController(rootViewController: favoritesViewController)
        
        self.viewControllers = [homeNavigationController,favoritesNavigationController]
    }
    
    func configureAppearance(){
        self.tabBar.unselectedItemTintColor = .white
        self.tabBar.tintColor =  lightGreen
        self.tabBar.barTintColor = dark
        self.tabBar.backgroundColor = dark
    }
    
}
