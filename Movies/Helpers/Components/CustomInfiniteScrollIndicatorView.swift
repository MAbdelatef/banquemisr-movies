

//
//  CustomInfiniteScrollIndicatorView.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CustomInfiniteScrollIndicatorView: UIView {
    
    var activityIndicatorView: NVActivityIndicatorView!

    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        configureIndicatorView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureIndicatorView(){
        activityIndicatorView = NVActivityIndicatorView(frame: self.frame)
        activityIndicatorView.tintColor = lightGreen!
        addSubview(activityIndicatorView)
    }
    
    @objc func startAnimating() {
        activityIndicatorView.startAnimating()
    }

    @objc func stopAnimationg() {
        activityIndicatorView.stopAnimating()
    }

}
