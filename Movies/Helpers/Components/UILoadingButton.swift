//
//  UILoadingButton.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/6/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class UILoadingButton: UIButton {
    
    var originalButtonText: String?
    var originalImage: UIImage?
    private var activityIndicator:UIActivityIndicatorView?
    
    func showLoading(color:UIColor = .lightGray){
        originalButtonText = self.titleLabel?.text
        originalImage = self.imageView?.image
        activityIndicator = UIActivityIndicatorView()
        activityIndicator!.hidesWhenStopped = true
        activityIndicator!.color = color
        activityIndicator!.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator!)
        activityIndicator?.startAnimating()
        setTitle(nil, for: .normal)
        setImage(nil, for: .normal)
    }
    
    func hideLoading(){
        setTitle(originalButtonText, for: .normal)
        setImage(originalImage, for: .normal)
        if let indicatorView = activityIndicator {
            indicatorView.stopAnimating()
        }
    }
}

extension UILoadingButton {
        
    private func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: self,
                                                   attribute: .centerX,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerX,
                                                   multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: self,
                                                   attribute: .centerY,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerY,
                                                   multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }
}
