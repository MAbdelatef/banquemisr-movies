//
//  EnviromentManager.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class EnviromentManager {
    
    static var shared = EnviromentManager()
    
    var baseURL: String {
        get{
            return  getValue(forKey: "BASE_URL")
        }
    }
    
    var apiKey: String{
        get{
            return  getValue(forKey: "API_KEY")
        }
    }
    
    var imageBaseURL: String{
        get{
            return  getValue(forKey: "IMAGE_BASE_URL")
        }
    }
    
    var apiAccessToken: String{
        get{
            return  getValue(forKey: "API_READ_ACCESS_TOKEN")
        }
    }
}

extension EnviromentManager {
    
    func getValue(forKey key:String) -> String{
        if let value = Bundle.main.infoDictionary?[key] as? String {
            return value
        }
        else{
            return ""
        }
    }
    
}
