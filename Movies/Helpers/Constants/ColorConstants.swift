//
//  ColorConstants.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
let dark  = UIColor(named: "Dark")
let darkBlue  = UIColor(named: "DarkBlue")
let lightBlue  = UIColor(named: "LightBlue")
let lightGreen  = UIColor(named: "LightGreen")
