//
//  AlertModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

class AlertModel {
    
    var title:String
    var message:String
    var cancelButtonTitle:String
    
    init(title:String,message:String,cancelButtonTitle:String = "Cancel") {
        self.title = title
        self.message = message
        self.cancelButtonTitle = cancelButtonTitle
    }
}
