//
//  EmptyStateViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

enum EmptyStateViewType: Int {
    case Loading = 1
    case Default = 2
}

class EmptyStateViewModel: NSObject {
    var type:EmptyStateViewType!
    var title:String?
    var stateDescription:String?
    
    var buttonActionClosure: (() -> Void)?
    
    init(type:EmptyStateViewType) {
        self.type = type
    }
    
    init(title:String? = nil, description:String? = nil) {
        self.type = .Default
        self.title = title
        self.stateDescription = description
    }
}
