//
//  NSObject Extension.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

extension NSObject {
    static var identifier: String {
        return String(describing: self)
    }
}
