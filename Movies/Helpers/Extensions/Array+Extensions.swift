//
//  Array+Extensions.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/8/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    func removingDuplicates() -> Array {
        return reduce(into: []) { result, element in
            if !result.contains(element) {
                result.append(element)
            }
        }
    }
}
