//
//  SharedEnums.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

enum LoadingType: Int {
    case Default = 1
    case Placeholder = 2
    case PullToRefresh = 3
    case LoadMore = 4
    case Custom = 5
}

enum RequestDataStatus{
    case NotStarted
    case InProgress
    case Completed
    case Error
}

enum NotificationName: String{
    case FavoriteStatusChanged = "FavoriteStatusChanged"
}
