//
//  ReuseIdentifiersProtocol.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

protocol CellReuseIdentifiers {
    
    func getCellReuseIdentifiers() -> [String]
}

protocol HeaderReuseIdentifiers {
    
    func getHeaderReuseIdentifiers() -> [String]
}
