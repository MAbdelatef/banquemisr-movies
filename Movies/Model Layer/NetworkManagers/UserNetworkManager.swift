//
//  UserNetworkManager.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxSwift

class UserNetworkManager: BaseNetworkManager {
    
    func createRequestToken() -> Observable<RequestTokenResponse> {
        return Observable<RequestTokenResponse>.create { observer in
            let router = UserRouter(endPoint: .createRequestToken)
            super.performNetworkRequest(forRouter: router, type: RequestTokenResponse.self, onSuccess: { (moviesResponse) in
                observer.onNext(moviesResponse as! RequestTokenResponse)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func createSessionWithLogin(username:String, password:String, requestToken:String) -> Observable<LoginResponse> {
        return Observable<LoginResponse>.create { observer in
            let router = UserRouter(endPoint: .login(username: username, password: password, requestToken: requestToken))
            super.performNetworkRequest(forRouter: router, type: LoginResponse.self, onSuccess: { (moviesResponse) in
                observer.onNext(moviesResponse as! LoginResponse)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func getSessionId(requestToken:String) -> Observable<CreateSessionResponse> {
        return Observable<CreateSessionResponse>.create { observer in
            let router = UserRouter(endPoint: .createSession(requestToken: requestToken))
            super.performNetworkRequest(forRouter: router, type: CreateSessionResponse.self, onSuccess: { (moviesResponse) in
                observer.onNext(moviesResponse as! CreateSessionResponse)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func getAccountDetails(sessionId:String) -> Observable<AccountDetails> {
        return Observable<AccountDetails>.create { observer in
            let router = UserRouter(endPoint: .getAccountDetails)
            super.performNetworkRequest(forRouter: router, type: AccountDetails.self, onSuccess: { (moviesResponse) in
                observer.onNext(moviesResponse as! AccountDetails)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func changeFavoriteStatusFor(movieId:Int, accountId:Int, isFavorite:Bool) -> Observable<StatusResponse> {
        Observable<StatusResponse>.create { observer in
            let router = UserRouter.init(endPoint: .changeFavoriteStatusFor(movieId: movieId, accountId: accountId, isFavorite: isFavorite))
            super.performNetworkRequest(forRouter: router, type: StatusResponse.self, onSuccess: { (moviesResponse) in
                observer.onNext(moviesResponse as! StatusResponse)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func getFavoriteMoviesFor(accountId:Int, page:Int = 1) -> Observable<GenericResponseModel<Movie>> {
        return Observable<GenericResponseModel<Movie>>.create { observer in
            let router = UserRouter(endPoint: .getFavoriteMoviesFor(accountId: accountId, page: page))
            super.performNetworkRequest(forRouter: router, type: GenericResponseModel<Movie>.self, onSuccess: { (response) in
                observer.onNext(response as! GenericResponseModel<Movie>)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
}
