//
//  MoviesNetworkManager.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/7/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxSwift

class MoviesNetworkManager: BaseNetworkManager {
    
    func loadMovieswith(pageIndex:Int) -> Observable<GenericResponseModel<Movie>> {
        return Observable<GenericResponseModel<Movie>>.create { observer in
            let router = MoviesRouter(endPoint: .getNowPlayingMovies(pageIndex: pageIndex))
            super.performNetworkRequest(forRouter: router, type: GenericResponseModel<Movie>.self, onSuccess: { (moviesResponse) in
                observer.onNext(moviesResponse as! GenericResponseModel<Movie>)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func getMovieDetailsWith(movieId:Int) -> Observable<movieDetail> {
        return Observable<movieDetail>.create { observer in
            let router = MoviesRouter(endPoint: .getMovieDetailsWith(Id: movieId))
            super.performNetworkRequest(forRouter: router, type: movieDetail.self, onSuccess: { (response) in
                observer.onNext(response as! movieDetail)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func getActorForMovieWith(Id:Int) -> Observable<MovieCredits> {
        return Observable<MovieCredits>.create { observer in
            let router = MoviesRouter(endPoint: .getActorsForMovieWith(Id: Id))
            super.performNetworkRequest(forRouter: router, type: MovieCredits.self, onSuccess: { (response) in
                observer.onNext(response as! MovieCredits)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func getRecommendationsForMovieWith(Id:Int,page:Int = 1) -> Observable<GenericResponseModel<Movie>> {
        return Observable<GenericResponseModel<Movie>>.create { observer in
            let router = MoviesRouter(endPoint: .getRecommendationsForMovieWith(Id: Id, pageIndex: page))
            super.performNetworkRequest(forRouter: router, type: GenericResponseModel<Movie>.self, onSuccess: { (response) in
                observer.onNext(response as! GenericResponseModel<Movie>)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func getSimilarMoviesForMovieWith(Id:Int, page:Int = 1) -> Observable<GenericResponseModel<Movie>> {
        return Observable<GenericResponseModel<Movie>>.create { observer in
            let router = MoviesRouter(endPoint: .getSimilarForMovieWith(Id: Id, pageIndex: page))
            super.performNetworkRequest(forRouter: router, type: GenericResponseModel<Movie>.self, onSuccess: { (response) in
                observer.onNext(response as! GenericResponseModel<Movie>)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
    
    func getAccountStatesFor(movieId:Int) -> Observable<MovieAccountStates> {
        return Observable<MovieAccountStates>.create { observer in
            let router = MoviesRouter(endPoint: .getAccountStatesFor(movieId: movieId))
            super.performNetworkRequest(forRouter: router, type: MovieAccountStates.self, onSuccess: { (response) in
                observer.onNext(response as! MovieAccountStates)
                observer.onCompleted()
                return
            }) { (error) in
                observer.onError(error)
                observer.onCompleted()
                return
            }
            return Disposables.create()
        }
    }
}
