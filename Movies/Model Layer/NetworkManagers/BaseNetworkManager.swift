//
//  BaseNetworkManager.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//


import Alamofire

typealias NetworkManagerRequest = DataRequest

class BaseNetworkManager: NSObject {
    
    typealias SuccessCompletion = (Any) -> Void
    
    typealias FailureCompletion = (Error) -> Void
    
    @discardableResult
    func performNetworkRequest<T:Codable>(forRouter router: BaseRouter , type:T.Type, onSuccess: @escaping SuccessCompletion , onFailure: @escaping FailureCompletion) -> DataRequest {
        
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            EnviromentManager.shared.baseURL : .performDefaultEvaluation(validateHost: true)
        ]
        let sessionManager = Alamofire.SessionManager(
            configuration: configuration,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        print("----- (router) ", (router) as Any)
        let request = sessionManager.request(router)
            .validate()
            .responseJSON { (response) in
                sessionManager.session.invalidateAndCancel()
                
                switch response.result {
                case .success( _):
                    do{
                        let json = response.result.value!
                        let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(type, from: jsonData)
                        return onSuccess(model)
                    }
                    catch{
                        return onFailure(error)
                    }
                    
                case .failure(let error):
                    return onFailure(error)
                }
        }
        return request
    }
}

