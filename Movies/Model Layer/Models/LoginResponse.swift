//
//  CreateSessionResponse.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

struct LoginResponse : Codable {

    let expiresAt : String?
    let requestToken : String?
    let statusCode : Int?
    let statusMessage : String?
    let success : Bool?
    
    enum CodingKeys: String, CodingKey {
        case expiresAt = "expires_at"
        case requestToken = "request_token"
        case statusCode = "status_code"
        case statusMessage = "status_message"
        case success = "success"
    }
}
