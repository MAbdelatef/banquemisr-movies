//
//  Person.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation


struct Actor : Codable {

    let castId : Int?
    let character : String?
    let creditId : String?
    let gender : Int?
    let id : Int?
    let name : String?
    let order : Int?
    let profilePath : String?
    
    enum CodingKeys: String, CodingKey {
        case castId = "cast_id"
        case character = "character"
        case creditId = "credit_id"
        case gender = "gender"
        case id = "id"
        case name = "name"
        case order = "order"
        case profilePath = "profile_path"
    }
}
