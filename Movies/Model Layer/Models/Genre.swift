//
//  Genre.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

struct Genre : Codable {

    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }

}
