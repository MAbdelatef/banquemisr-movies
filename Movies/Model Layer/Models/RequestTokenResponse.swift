//
//  RequestTokenResponse.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

struct RequestTokenResponse : Codable {

    let expiresAt : String?
    let requestToken : String?
    let success : Bool?

    enum CodingKeys: String, CodingKey {
        case expiresAt = "expires_at"
        case requestToken = "request_token"
        case success = "success"
    }
}
