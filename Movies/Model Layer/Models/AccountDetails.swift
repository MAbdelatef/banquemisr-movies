//
//  AccountDetails.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

struct AccountDetails : Codable {

    let id : Int?
    let includeAdult : Bool?
    let iso31661 : String?
    let iso6391 : String?
    let name : String?
    let username : String?


    enum CodingKeys: String, CodingKey {
        case id = "id"
        case includeAdult = "include_adult"
        case iso31661 = "iso_3166_1"
        case iso6391 = "iso_639_1"
        case name = "name"
        case username = "username"
    }
}
