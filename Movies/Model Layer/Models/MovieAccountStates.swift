
//
//  MovieAccountStates.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/10/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

struct MovieAccountStates : Codable {

    let favorite : Bool?
    let id : Int?
    let rated : Bool?
    let watchlist : Bool?
}
