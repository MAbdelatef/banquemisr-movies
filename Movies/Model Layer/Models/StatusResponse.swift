//
//  StatusResponse.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/10/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

struct StatusResponse : Codable {

    let statusCode : Int?
    let statusMessage : String?

    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case statusMessage = "status_message"
    }
}
