//
//  GenericResponseModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/7/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

struct GenericResponseModel<T: Codable>: Codable {
    
    let page : Int?
    let results : [T]?
    let totalPages : Int?
    let totalResults : Int?


    enum CodingKeys: String, CodingKey {
        case page = "page"
        case results = "results"
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        page = try values.decodeIfPresent(Int.self, forKey: .page)
        results = try values.decodeIfPresent([T].self, forKey: .results)
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages)
        totalResults = try values.decodeIfPresent(Int.self, forKey: .totalResults)
    }
}
