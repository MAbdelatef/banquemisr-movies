//
//  CreateSessionResponse.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

struct CreateSessionResponse : Codable {

    let sessionId : String?
    let success : Bool?

    enum CodingKeys: String, CodingKey {
        case sessionId = "session_id"
        case success = "success"
    }
}
