//
//  MoviesRouter.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/7/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import Alamofire

enum MoviesEndPoint {
    case getNowPlayingMovies(pageIndex: Int)
    case getMovieDetailsWith(Id: Int)
    case getActorsForMovieWith(Id: Int)
    case getRecommendationsForMovieWith(Id: Int, pageIndex: Int)
    case getSimilarForMovieWith(Id: Int, pageIndex: Int)
    case getAccountStatesFor(movieId:Int)
}

class MoviesRouter: BaseRouter {
    var endPoint:MoviesEndPoint
    
    init(endPoint:MoviesEndPoint) {
        self.endPoint = endPoint
    }
    
    override var method: HTTPMethod{
        switch endPoint {
        case .getNowPlayingMovies,
             .getMovieDetailsWith,
             .getAccountStatesFor,
             .getActorsForMovieWith,
             .getSimilarForMovieWith,
             .getRecommendationsForMovieWith:
            return .get
        }
    }
    
    override var path: String{
        switch endPoint {
        case .getNowPlayingMovies:
            return "/movie/now_playing"
        case .getMovieDetailsWith(let Id):
            return "/movie/\(Id)"
        case .getAccountStatesFor(let Id):
            return "/movie/\(Id)/account_states"
        case .getActorsForMovieWith(let Id):
            return "/movie/\(Id)/credits"
        case .getRecommendationsForMovieWith(let Id, _):
            return "/movie/\(Id)/recommendations"
        case .getSimilarForMovieWith(let Id, _):
            return "/movie/\(Id)/similar"
        }
    }
    
    override var encoding: ParameterEncoding?{
        
        switch method {
        case .post:
            return JSONEncoding.default
        default:
            return URLEncoding.default
            
        }
    }
    
    override var parameters: APIParams{
        switch endPoint {
        case .getNowPlayingMovies(let page),
             .getRecommendationsForMovieWith(_, let page),
             .getSimilarForMovieWith(_, let page):
            return [
                "page":page as AnyObject,
            ]
        default:
            return [:]
        }
    }
    
    override var defaultQueryParameters: [String : AnyObject]{
        switch endPoint {
        case .getAccountStatesFor:
            guard let sessionId = ApplicationUserManager.shared.sessionId else { return super.defaultQueryParameters}
            return [
                "api_key":EnviromentManager.shared.apiKey as AnyObject,
                "session_id":sessionId as AnyObject,
            ]
        default:
            return super.defaultQueryParameters
        }
    }
}
