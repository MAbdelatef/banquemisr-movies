//
//  UserRouter.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import Alamofire

enum UserDataEndPoint {
    case createRequestToken
    case login(username:String,password:String, requestToken:String)
    case createSession(requestToken:String)
    case getAccountDetails
    case changeFavoriteStatusFor(movieId:Int, accountId:Int, isFavorite:Bool)
    case getFavoriteMoviesFor(accountId:Int, page:Int)
}


class UserRouter: BaseRouter {
    var endPoint:UserDataEndPoint
    
    init(endPoint:UserDataEndPoint) {
        self.endPoint = endPoint
    }
    
    override var method: HTTPMethod{
        switch endPoint {
        case .login, .createSession, .changeFavoriteStatusFor:
            return .post
        case .createRequestToken, .getAccountDetails, .getFavoriteMoviesFor:
            return .get
        }
    }
    
    override var path: String{
        switch endPoint {
        case .createRequestToken:
            return "/authentication/token/new"
        case .login:
            return "/authentication/token/validate_with_login"
        case .createSession:
            return "/authentication/session/new"
        case .getAccountDetails:
            return "/account"
        case .changeFavoriteStatusFor(_, let accountId, _):
            return "/account/\(accountId)/favorite"
        case .getFavoriteMoviesFor(let accountId, _):
            return "/account/\(accountId)/favorite/movies"
        }
    }
    
    override var encoding: ParameterEncoding?{
        
        switch method {
        case .post:
            return JSONEncoding.default
        default:
            return URLEncoding.default
            
        }
    }
    
    override var parameters: APIParams{
        switch endPoint {
        case .login(let username, let password, let requestToken):
            return [
                "username": username as AnyObject,
                "password": password as AnyObject,
                "request_token": requestToken as AnyObject
            ]
        case .createSession(let requestToken):
            return [
                "request_token": requestToken as AnyObject
            ]
        case .changeFavoriteStatusFor(let movieId, _, let isFavorite):
            return [
                "media_type":"movie" as AnyObject,
                "media_id":movieId as AnyObject,
                "favorite":isFavorite as AnyObject,
            ]
        case .getFavoriteMoviesFor(_, let page):
            return [
                "page":page as AnyObject
            ]
            
        default:
            return [:]
        }
    }
    
    override var defaultQueryParameters: [String : AnyObject]{
        switch endPoint {
        case .getAccountDetails,
             .changeFavoriteStatusFor,
             .getFavoriteMoviesFor:
            guard let sessionId = ApplicationUserManager.shared.sessionId else { return super.defaultQueryParameters}
            return [
                "api_key":EnviromentManager.shared.apiKey as AnyObject,
                "session_id":sessionId as AnyObject,
            ]
        default:
            return [
                "api_key":EnviromentManager.shared.apiKey as AnyObject
            ]
        }
    }
    
}

