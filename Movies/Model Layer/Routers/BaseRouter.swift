//
//  BaseRouter.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import Alamofire

public typealias JSONDictionary = [String: AnyObject]
typealias APIParams = [String : AnyObject]?

protocol APIConfiguration {
    var method: Alamofire.HTTPMethod { get }
    var encoding: Alamofire.ParameterEncoding? { get }
    var path: String { get }
    var parameters: APIParams { get }
    var baseURLString: String { get }
    var requestHeaders : [String : Any] { get }
}

class BaseRouter : URLRequestConvertible, APIConfiguration {
    
    init() {}
    
    var method: Alamofire.HTTPMethod {
        fatalError("[\(Mirror(reflecting: self).description) - \(#function))] Must be overridden in subclass")
    }
    
    var encoding: Alamofire.ParameterEncoding? {
        fatalError("[\(Mirror(reflecting: self).description) - \(#function))] Must be overridden in subclass")
    }
    
    var path: String {
        fatalError("[\(Mirror(reflecting: self).description) - \(#function))] Must be overridden in subclass")
    }
    
    var parameters: APIParams {
        fatalError("[\(Mirror(reflecting: self).description) - \(#function))] Must be overridden in subclass")
    }
    
    var baseURLString: String {
        return EnviromentManager.shared.baseURL
    }
    
    var requestHeaders : [String : Any]{
        return [
            "Content-Type" : "application/json",
            "Accept" : "application/json",
        ]
    }
    
    var defaultQueryParameters: [String:AnyObject] {
        return [
            "api_key":EnviromentManager.shared.apiKey as AnyObject
        ]
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let url = URL(string: baseURLString)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))

        try  urlRequest = URLEncoding.default.encode(urlRequest, with: defaultQueryParameters)
        
        for (key,value) in requestHeaders {
            urlRequest.setValue(value as? String, forHTTPHeaderField: key)
        }
        
        urlRequest.httpMethod = method.rawValue
        if encoding != nil {
            urlRequest = try encoding!.encode(urlRequest, with: parameters)
            print(urlRequest)
            return urlRequest
        }
        else {
            return urlRequest
        }
    }
}


