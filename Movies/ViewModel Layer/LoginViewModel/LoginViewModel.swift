//
//  LoginViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxRelay
import RxSwift

class LoginViewModel: BaseViewModel {
    
    // MARK: - variables
    let username: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    let password: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    let showTabBar = PublishSubject<Void>()
    let networkManager = UserNetworkManager()
    
    override init() {
        username.accept("MAbden")
        password.accept("test1234")
    }
    
    // MARK: - actions
    func handleLoginAction(){
        guard let username = username.value, let password = password.value, !username.isEmpty, !password.isEmpty else {
            let alertViewModel = AlertModel(title: "Validation Error", message: "Please enter Both email and password")
            showAlertSubject.onNext(alertViewModel)
            return
        }
        
        login()
    }
    
    func login(){
        self.showLoading(loadingType: .Default)
        ///Create Request Token
        networkManager.createRequestToken().asObservable()
            .flatMap {
                
                [weak self] (requesTokenResponse) -> Observable<LoginResponse> in
                guard let self = self else { return Observable.empty() }
                guard let requestToken = requesTokenResponse.requestToken else { return Observable.empty() }
                return self.networkManager.createSessionWithLogin(username: "MAbden", password: "test1234", requestToken: requestToken).asObservable() }
            ///Login with User Name and Password
            .flatMap { [weak self] (loginResponse) -> Observable<CreateSessionResponse> in
                guard let self = self else { return Observable.empty() }
                guard let success = loginResponse.success, let requestToken = loginResponse.requestToken,success else { return Observable.empty() }
                return self.networkManager.getSessionId(requestToken: requestToken).asObservable() }
            
            ///Get Session Id
            .flatMap { [weak self] (createSessionResponse) -> Observable<AccountDetails> in
                
                guard let self = self else { return Observable.empty() }
                guard let sessionId = createSessionResponse.sessionId else { return Observable.empty() }
                ApplicationUserManager.shared.sessionId = sessionId
                return self.networkManager.getAccountDetails(sessionId: sessionId).asObservable()
            ///Get Account Details
        }.asObservable().subscribe(onNext: { (accountDetails) in
            
            guard let accountId = accountDetails.id else { return  }
            ApplicationUserManager.shared.accoountId = accountId
            self.hideLoading(loadingType: .Default)
            self.showTabBar.onNext(())
            
        }, onError: { [weak self] (error) in
            
            guard let self = self else { return }
            self.hideLoading(loadingType: .Default)
            self.showErrorAlertWith(message: error.localizedDescription)
            
        }).disposed(by: bag)
    }
}
