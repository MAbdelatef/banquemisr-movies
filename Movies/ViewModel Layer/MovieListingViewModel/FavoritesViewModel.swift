//
//  FavoritesViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

class FavoritesViewModel: MovieListingViewModel {
    
    // MARK: - variables
    let networkManager = UserNetworkManager()
    
    // MARK: - helpers
    override func commonInit(){
        self.tabBarItemViewModel = TabBarItemViewModel(title: "Favorites", iconName: "icn_tab_favorite")
        viewTitle = "Favorites"
        NotificationCenter.default.addObserver(self, selector: #selector(loadIntialData), name: Notification.Name(NotificationName.FavoriteStatusChanged.rawValue), object: nil)
    }
    
    // MARK: - data configuration
    override func loadDataWith(loadingType: LoadingType) {
        guard let _ = ApplicationUserManager.shared.sessionId, let accountId = ApplicationUserManager.shared.accoountId else {
            showEmptyStateWith(message: "Please login to be able to view your favorites")
            return
        }
        showLoading(loadingType: loadingType)
        networkManager.getFavoriteMoviesFor(accountId: accountId)
            .asObservable()
            .subscribe(onNext: { [weak self] (response) in
                guard let self = self else { return }
                self.hideLoading(loadingType: loadingType)
                self.configureDataSource(response: response)
                },onError: { [weak self] (error) in
                    guard let self = self else { return }
                    self.hideLoading(loadingType: loadingType)
            }).disposed(by: bag)
    }
    
}
