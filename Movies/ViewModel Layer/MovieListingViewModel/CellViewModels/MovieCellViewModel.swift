//
//  MovieCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxRelay

class MovieCellViewModel: BaseCellViewModel {
    
    // MARK: - variables
    private var movieModel:Movie
    var moviePosterURL:String?
    var movieTitle:String = ""
    var releaseDate:String = ""
    var rateText:String = "-"
    var stars:Double = 0
    var movieId:Int = 0
    
    override var cellIdentifier: String{
        get {return MovieTableViewCell.identifier}
        set {}
    }
    
    // MARK: - intialization
    init(movie:Movie) {
        self.movieModel = movie
        super.init()
        configureData()
    }
    
    // MARK: - data configuration
    func configureData(){
        movieId = movieModel.id ?? 0
        
        if let posterPath = movieModel.posterPath{
            moviePosterURL = EnviromentManager.shared.imageBaseURL + posterPath
        }
        
        movieTitle = movieModel.title ?? "-"
        releaseDate = movieModel.releaseDate ?? "-"
        
        if let voteAverage = movieModel.voteAverage{
            stars = ((voteAverage/100)*maximumRate)*maximumStars
            rateText = "\(voteAverage)"
        }

    }
}
