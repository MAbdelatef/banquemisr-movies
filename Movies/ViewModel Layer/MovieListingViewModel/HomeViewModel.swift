//
//  HomeViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

class HomeViewModel: MovieListingViewModel {
    
    // MARK: - variables
    let networkManager = MoviesNetworkManager()
    
    // MARK: - helpers
    override func commonInit(){
        super.commonInit()
        self.tabBarItemViewModel = TabBarItemViewModel(title: "Home", iconName: "icn_tab_home")
        viewTitle = "Now Playing"
    }
    
    // MARK: - data configuration
    override func loadDataWith(loadingType: LoadingType) {
        showLoading(loadingType: loadingType)
        networkManager.loadMovieswith(pageIndex: pageIndex)
            .asObservable()
            .subscribe(onNext: { [weak self] (response) in
                guard let self = self else { return }
                self.hideLoading(loadingType: loadingType)
                self.configureDataSource(response: response)
                },onError: { [weak self] (error) in
                    guard let self = self else { return }
                    self.hideLoading(loadingType: loadingType)
            }).disposed(by: bag)
    }
}
