//
//  MovieListingViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/6/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxRelay
import RxSwift

class MovieListingViewModel: BaseViewModel {

    // MARK: - variables
    var pageIndex:Int = 1
    var loadMoreEnabled: Bool =  true
    let dataSource: BehaviorRelay<[MovieCellViewModel]> = BehaviorRelay(value: [])
    let showMovieDetails = PublishSubject<MovieDetailsViewModel>()
    
    // MARK: - initialization
    override init() {
        super.init()
        commonInit()
    }
    
    // MARK: - helpers
    func commonInit(){
        
    }
    
    // MARK: - data Configuration
    @objc func loadIntialData(){
        pageIndex = 1
        loadDataWith(loadingType: .Placeholder)
    }
    
    func loadMore(){
         pageIndex+=1
         loadDataWith(loadingType: .LoadMore)
     }
    
    func loadDataWith(loadingType:LoadingType){
        
    }
    
    func configureDataSource(response:GenericResponseModel<Movie>) {
        self.configureLoadMoreEnabledFlag(totalPages: response.totalPages)
        if let movies = response.results{
            let cellViewModels = movies.map { (movie) -> MovieCellViewModel in
                return MovieCellViewModel(movie: movie)
            }
            if pageIndex == 1 {
                dataSource.accept(cellViewModels)
            }
            else{
                dataSource.accept(dataSource.value + cellViewModels)
            }
        }
        if pageIndex == 1 && dataSource.value.count == 0 {
            showEmptyStateWith(message: "No Results Found")
        }
    }
    
    func configureLoadMoreEnabledFlag(totalPages:Int?){
        if let totalPages = totalPages{
            self.loadMoreEnabled = totalPages>self.pageIndex
        }
        else{
            self.loadMoreEnabled = false
        }
    }
    
    // MARK: - actions
    func handlePullToRefresh(){
        pageIndex = 1
        loadDataWith(loadingType: .PullToRefresh)
    }
    
    func handleSelectionFor(cellViewModel:BaseCellViewModel){
        if let viewModel = cellViewModel as? MovieCellViewModel{
            let detailsViewModel = MovieDetailsViewModel(movieId: viewModel.movieId, viewTitle: viewModel.movieTitle)
            showMovieDetails.onNext(detailsViewModel)
        }
    }
}
