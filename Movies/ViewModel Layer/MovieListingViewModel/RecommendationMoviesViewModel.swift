//
//  RecommendationMoviesViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class RecommendationMoviesViewModel: MovieListingViewModel {
    
    // MARK: - variables
    private var movieId:Int
    let networkManager = MoviesNetworkManager()
    
    // MARK: - initializations
    init(movieId:Int) {
        self.movieId = movieId
    }
    
    // MARK: - helpers
    override func commonInit(){
        viewTitle = "Recommendations"
    }
    
    // MARK: - data configuration
    override func loadDataWith(loadingType: LoadingType) {
        showLoading(loadingType: loadingType)
        networkManager.getRecommendationsForMovieWith(Id: movieId, page: pageIndex)
        .asObservable()
        .subscribe(onNext: { [weak self] (response) in
            guard let self = self else { return }
            self.hideLoading(loadingType: loadingType)
            self.configureDataSource(response: response)
            },onError: { [weak self] (error) in
                guard let self = self else { return }
                self.hideLoading(loadingType: loadingType)
        }).disposed(by: bag)
    }

}
