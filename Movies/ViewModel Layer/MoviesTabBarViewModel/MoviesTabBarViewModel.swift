//
//  MoviesTabBarViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/6/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

class MoviesTabBarViewModel: BaseViewModel {
    
    // MARK: - variables
    let homeViewModel  = HomeViewModel()
    let favoritesViewModel = FavoritesViewModel()
}
