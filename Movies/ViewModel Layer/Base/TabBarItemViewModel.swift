//
//  TabBarItemViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/6/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

class TabBarItemViewModel {
    
    // MARK: - variables
    var title:String
    var iconName:String
    
    // MARK: - initialization
    init(title:String, iconName:String) {
        self.title = title
        self.iconName = iconName
    }
}
