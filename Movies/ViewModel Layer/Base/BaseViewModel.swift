//
//  BaseViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxSwift

class BaseViewModel: NSObject {
    
    // MARK: - variables
    var isLoading:Bool = false
    var tabBarItemViewModel:TabBarItemViewModel?
    var viewTitle:String = ""
    let bag = DisposeBag()
    
    let showLoadingSubject = PublishSubject<LoadingType>()
    let hideLoadingSubject = PublishSubject<LoadingType>()
    
    var showEmptyStateSubject = PublishSubject<EmptyStateViewModel>()
    var hideEmptyStateSubject = PublishSubject<Void>()
    
    var popViewControllerSubject = PublishSubject<Void>()
    var dismissViewControllerSubject = PublishSubject<Void>()
    
    let showAlertSubject = PublishSubject<AlertModel>()
    
    // MARK: - helpers
    func showLoading(loadingType:LoadingType){
        if !isLoading {
            if loadingType == .Default{
                showLoadingSubject.onNext(.Default)
            }
            else if loadingType == .Placeholder{
                showEmptyStateWith(viewModel: EmptyStateViewModel(type: .Loading))
            }
            isLoading = true
        }
    }
    
    func hideLoading(loadingType:LoadingType){
        if loadingType == .Placeholder{
            hideEmptyStateSubject.onNext(())
        }
        else{
            hideLoadingSubject.onNext(loadingType)
        }
        isLoading = false
    }
    
    func showEmptyStateWith(viewModel:EmptyStateViewModel){
        showEmptyStateSubject.onNext(viewModel)
    }
    
    func showEmptyStateWith(message: String) {
        let emptyStateViewModel = EmptyStateViewModel(title: message, description: nil)
        showEmptyStateWith(viewModel: emptyStateViewModel)
    }
    
    func hideEmptyState(){
        hideEmptyStateSubject.onNext(())
    }
    
    func popView(){
        popViewControllerSubject.onNext(())
    }
    
    func dismissView(){
        dismissViewControllerSubject.onNext(())
    }
    
    func showErrorAlertWith(message:String) {
        let alertViewModel = AlertModel(title: "Error", message: message)
        showAlertSubject.onNext(alertViewModel)
    }
    
    deinit {
        print(String(describing: self) + "deinit")
    }
}
