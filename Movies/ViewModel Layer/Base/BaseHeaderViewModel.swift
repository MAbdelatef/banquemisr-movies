//
//  TableViewSectionViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/8/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxSwift

class BaseHeaderViewModel {
    
    // MARK: - variables
    var headerIdentifier:String = ""
    var cellViewModels: [BaseCellViewModel] = []
    var headerEnabled:Bool = false
    
    // MARK: - helper
    func headerHeight() -> CGFloat {
          return 0.0
    }
    
}
