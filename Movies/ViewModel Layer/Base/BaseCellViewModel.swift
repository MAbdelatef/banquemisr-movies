//
//  BaseCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxSwift

class BaseCellViewModel {
    
    // MARK: - variables
    var cellIdentifier:String = ""
    var bag = DisposeBag()
    var isLoading:Bool = false
    
    let showLoadingSubject = PublishSubject<LoadingType>()
    let hideLoadingSubject = PublishSubject<LoadingType>()
    
    var showEmptyStateSubject = PublishSubject<EmptyStateViewModel>()
    var hideEmptyStateSubject = PublishSubject<Void>()
    
    // MARK: - Helpers
    func showLoading(loadingType:LoadingType){
        if !isLoading {
            if loadingType == .Placeholder{
                showEmptyStateWith(viewModel: EmptyStateViewModel(type: .Loading))
            }
            else {
                self.showLoadingSubject.onNext(loadingType)
            }
            self.isLoading = true
        }
    }
    
    func hideLoading(loadingType:LoadingType){
        if loadingType == .Placeholder{
            hideEmptyState()
        }
        else {
            hideLoadingSubject.onNext(loadingType)
        }
        self.isLoading = false
    }
    
    func showEmptyStateWith(viewModel:EmptyStateViewModel){
        self.showEmptyStateSubject.onNext(viewModel)
    }
    
    func hideEmptyState(){
        hideEmptyStateSubject.onNext(())
    }
}
