//
//  GenericCollectionCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxSwift

class GenericCollectionCellViewModel: BaseCellViewModel {
    
    // MARK: - variables
    var dataSource:[BaseCellViewModel] = []
    let reloadSubject = PublishSubject<Void>()
    var dataStatus:RequestDataStatus = .NotStarted
    
    
    override var cellIdentifier: String{
        get {return GenericCollectionTableViewCell.identifier}
        set {}
    }
    
    // MARK: - Helpers
    func configureData(){
        if dataStatus == .NotStarted || dataStatus == .Error{
            loadDataFromAPI()
        }
        else{
            reloadSubject.onNext(())
        }
    }
    
    func loadDataFromAPI(){
        
    }
    
    func getItemSize() -> CGSize{
        return CGSize(width: 160, height: 250)
    }
    
    func showEmptyStateWith(message: String) {
        let emptyStateViewModel = EmptyStateViewModel(title: message, description: nil)
        showEmptyStateWith(viewModel: emptyStateViewModel)
    }
    
    // MARK: - actions
    func handleSelectionFor(index:Int){
        
    }
}

extension GenericCollectionCellViewModel: CellReuseIdentifiers{
    
    func getCellReuseIdentifiers() -> [String] {
        return dataSource.map({$0.cellIdentifier})
    }
}

