//
//  ActorsCollectionCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class ActorsListCellViewModel: GenericCollectionCellViewModel {
    
    // MARK: - variables
    var movieId:Int
    var networkManager = MoviesNetworkManager()
    
    // MARK: - intialization
    init(movieId:Int) {
        self.movieId = movieId
    }
    
    // MARK: - Data Configurtion
    override func loadDataFromAPI() {
        showLoading(loadingType: .Placeholder)
        dataStatus = .InProgress
        networkManager.getActorForMovieWith(Id: movieId)
            .asObservable()
            .subscribe(onNext: { [weak self] (response) in
                guard  let self = self else { return }
                self.hideLoading(loadingType: .Placeholder)
                self.dataStatus = .Completed
                self.configureDataSource(credits: response)
            }, onError: { [weak self] (error) in
                guard  let self = self else { return }
                self.hideLoading(loadingType: .Placeholder)
                self.dataStatus = .Error
                self.showEmptyStateWith(message: error.localizedDescription)
            }).disposed(by: bag)
    }
    
    func configureDataSource(credits:MovieCredits){
        if let actors = credits.cast, actors.count > 0{
            dataSource = actors.map { (actor) -> ActorCollectionCellViewModel in
                return ActorCollectionCellViewModel(actor: actor)
            }
            reloadSubject.onNext(())
        }
        else{
            self.showEmptyStateWith(message: "No Actors Found")
        }
    }
    
}
