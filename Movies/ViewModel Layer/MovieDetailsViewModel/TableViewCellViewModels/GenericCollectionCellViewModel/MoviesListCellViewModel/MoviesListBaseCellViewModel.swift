//
//  MoviesListBaseCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxSwift

class MoviesListBaseCellViewModel: GenericCollectionCellViewModel {
    
    // MARK: - variables
    var movieId:Int
    var networkManager = MoviesNetworkManager()
    var showMovieDetails = PublishSubject<MovieDetailsViewModel>()
    
    // MARK: - intialization
    init(movieId:Int) {
        self.movieId = movieId
    }
    
    func configureDataSource(respose:GenericResponseModel<Movie>){
        if let movies = respose.results, movies.count > 0{
            dataSource = movies.map { (movie) -> MovieCollectionCellViewModel in
                return MovieCollectionCellViewModel(movie: movie)
            }
            reloadSubject.onNext(())
        }
        else{
            showEmptyStateWith(message: "No Results Found")
        }
    }
    
    // MARK: - actions
    override func handleSelectionFor(index:Int){
        let cellViewModel = dataSource[index] as! MovieCollectionCellViewModel
        let movieDetailsViewModel = MovieDetailsViewModel(movieId: cellViewModel.movieId, viewTitle: cellViewModel.title)
        showMovieDetails.onNext(movieDetailsViewModel)
    }

}
