//
//  RecommendedMoviesListCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit
import RxSwift

class RecommendedMoviesListCellViewModel: MoviesListBaseCellViewModel {
    
    // MARK: - Data Configurtion
    override func loadDataFromAPI() {
        showLoading(loadingType: .Placeholder)
        dataStatus = .InProgress
        networkManager.getRecommendationsForMovieWith(Id: movieId)
            .asObservable()
            .subscribe(onNext: { [weak self] (response) in
                guard  let self = self else { return }
                self.hideLoading(loadingType: .Placeholder)
                self.dataStatus = .Completed
                self.configureDataSource(respose: response)
                }, onError: { [weak self] (error) in
                    guard  let self = self else { return }
                    self.hideLoading(loadingType: .Placeholder)
                    self.dataStatus = .Error
                    self.showEmptyStateWith(message: error.localizedDescription)
            }).disposed(by: bag)
    }
}
