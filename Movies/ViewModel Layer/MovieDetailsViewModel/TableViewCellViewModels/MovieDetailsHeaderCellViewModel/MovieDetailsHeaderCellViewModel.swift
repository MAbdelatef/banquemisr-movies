//
//  MovieDetailsHeaderCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxRelay

class MovieDetailsHeaderCellViewModel: BaseCellViewModel {
    
    // MARK: - variables
    private var movieDetailsModel:movieDetail
    private var movieAccountstates:MovieAccountStates?
    
    var moviePosterURL:String?
    var movieTitle:String = ""
    var year:String = "-"
    var rateText:String = "-"
    var stars:Double = 0
    let genreDataSource: BehaviorRelay<[GenreCellViewModel]> = BehaviorRelay(value: [])
    let isFavorite: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let networkManager = UserNetworkManager()
    
    override var cellIdentifier: String{
        get {return MovieDetailsHeaderTableViewCell.identifier}
        set {}
    }
    
    // MARK: - intialization
    init(movie:movieDetail,states:MovieAccountStates? = nil) {
        self.movieDetailsModel = movie
        self.movieAccountstates = states
        super.init()
        configureData()
    }
    
    // MARK: - data configuration
    func configureData(){
        if let posterPath = movieDetailsModel.posterPath{
            moviePosterURL = EnviromentManager.shared.imageBaseURL + posterPath
        }
        movieTitle = movieDetailsModel.title ?? "-"
        
        if let dateString = movieDetailsModel.releaseDate {
            if let releaseDate = DateFormatter.defaultDateFormate.date(from: dateString){
                year = "\(releaseDate.get(.year))"
            }
        }
        
        if let voteAverage = movieDetailsModel.voteAverage{
            stars = ((voteAverage/100)*maximumRate)*maximumStars
            rateText = "\(voteAverage)"
        }
        
        if let genres = movieDetailsModel.genres{
            let cellViewModels = genres.map { (model) -> GenreCellViewModel in
                return GenreCellViewModel(genreModel: model)
            }
            genreDataSource.accept(cellViewModels)
        }
        
        if let movieAccountStates = movieAccountstates, let favorite = movieAccountStates.favorite{
            isFavorite.accept(favorite)
        }
    }
    
    // MARK: - Actions
    func handleFavoriteAction(){
        
        guard let movieId = movieDetailsModel.id, let accountId = ApplicationUserManager.shared.accoountId else { return }
        let newStatus = !isFavorite.value
        showLoading(loadingType: .Custom)
        networkManager.changeFavoriteStatusFor(movieId: movieId, accountId: accountId, isFavorite: newStatus)
            .asObservable()
            .subscribe(onNext: { [weak self] (response) in
                guard let self = self else { return }
                self.hideLoading(loadingType: .Custom)
                self.isFavorite.accept(newStatus)
                NotificationCenter.default.post(name: Notification.Name(NotificationName.FavoriteStatusChanged.rawValue), object: nil)
                }, onError: { [weak self] (error) in
                    guard let self = self else { return }
                    self.hideLoading(loadingType: .Custom)
            }).disposed(by: bag)
    }
    
}
