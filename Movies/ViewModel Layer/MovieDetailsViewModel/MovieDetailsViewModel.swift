

//
//  MovieDetailsViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/5/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxSwift

class MovieDetailsViewModel: BaseViewModel {
    
    // MARK: - variables
    let networkManager = MoviesNetworkManager()
    var movieDetail:movieDetail?
    var accountStates:MovieAccountStates?
    var movieId:Int
    var dataSource:[BaseHeaderViewModel] = []
    let reloadSubject = PublishSubject<Void>()
    let showMovieDetails = PublishSubject<MovieDetailsViewModel>()
    let showMoviesListing = PublishSubject<MovieListingViewModel>()
    
    // MARK: - initialization
    init(movieId:Int, viewTitle:String) {
        self.movieId = movieId
        super.init()
        self.viewTitle = viewTitle
    }
    
    // MARK: - data configuration
    func loadData(){
        showLoading(loadingType: .Placeholder)
        networkManager.getMovieDetailsWith(movieId: movieId).asObservable().flatMap { [weak self] (movieDetails) -> Observable<MovieAccountStates> in
            guard let self = self else { return Observable.empty()}
            self.movieDetail = movieDetails
            guard let _ = ApplicationUserManager.shared.sessionId, let _ = ApplicationUserManager.shared.accoountId else {
                return Observable.empty()
            }
            return self.networkManager.getAccountStatesFor(movieId: self.movieId).asObservable()
        }.asObservable().subscribe(onNext: { [weak self] (accountStates) in
            guard let self = self else { return }
            self.accountStates = accountStates
            self.hideLoading(loadingType: .Placeholder)
            self.confiugreDataSource()
        }, onError: { [weak self] (error) in
            guard let self = self else { return }
            self.hideLoading(loadingType: .Placeholder)
            self.showEmptyStateWith(message: error.localizedDescription)
        }).disposed(by: bag)
    }
    
    func confiugreDataSource(){
        guard let movieDetail = movieDetail, let movieId = movieDetail.id else { return }
        
        let headerCellViewModel = MovieDetailsHeaderCellViewModel(movie: movieDetail,states: accountStates)
        let headerSectionViewModel = MovieDetailsHeaderViewModel(viewModels: [headerCellViewModel])
        
        let actorsCellViewModel = ActorsListCellViewModel(movieId: movieId)
        let actorsSectionViewModel = MovieDetailsHeaderViewModel(viewModels: [actorsCellViewModel], title: "Cast", headerEnabled: true)
        
        let similarMoviesCellViewModel = SimilarMoviesListCellViewModel(movieId: movieId)
        let similarMoviesSectionViewModel = MovieDetailsHeaderViewModel(viewModels: [similarMoviesCellViewModel], title: "Similar Movies", headerEnabled: true, buttonTitle: "See All")
        similarMoviesSectionViewModel.buttonActionSubject.asObservable().subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.showMoviesListing.onNext(SimilarMoviesViewmodel(movieId: movieId))
        }).disposed(by: bag)
        
        let recommendedMoviesCellViewModel = RecommendedMoviesListCellViewModel(movieId: movieId)
        let recommendedMoviesSectionViewModel = MovieDetailsHeaderViewModel(viewModels: [recommendedMoviesCellViewModel], title: "Recommendations", headerEnabled: true, buttonTitle: "See All")
        recommendedMoviesSectionViewModel.buttonActionSubject.asObservable().subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.showMoviesListing.onNext(RecommendationMoviesViewModel(movieId: movieId))
        }).disposed(by: bag)
        
        Observable.of(similarMoviesCellViewModel.showMovieDetails,recommendedMoviesCellViewModel
            .showMovieDetails).merge().asObservable()
            .subscribe(onNext: { [weak self] (detailsViewModel) in
                guard let self = self else { return }
                self.showMovieDetails.onNext(detailsViewModel)
            }).disposed(by: bag)
        dataSource = [headerSectionViewModel, actorsSectionViewModel, similarMoviesSectionViewModel, recommendedMoviesSectionViewModel]
        reloadSubject.onNext(())
    }
}

extension MovieDetailsViewModel: CellReuseIdentifiers, HeaderReuseIdentifiers{
    
    func getCellReuseIdentifiers() -> [String] {
        var reuseIdentifiers:[String] = []
        dataSource.forEach { (section) in
            let sectionCellsReuseIdentifiers = section.cellViewModels.map({$0.cellIdentifier})
            reuseIdentifiers.append(contentsOf: sectionCellsReuseIdentifiers)
        }
        return reuseIdentifiers.removingDuplicates()
    }
    
    func getHeaderReuseIdentifiers() -> [String] {
        return dataSource.map({$0.headerIdentifier})
    }
}
