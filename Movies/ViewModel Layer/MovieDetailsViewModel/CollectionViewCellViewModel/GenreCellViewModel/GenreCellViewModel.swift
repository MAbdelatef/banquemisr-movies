//
//  GenreCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation

class GenreCellViewModel: BaseCellViewModel {
    
    // MARK: - variables
    var genretTitle:String = ""
    override var cellIdentifier: String{
        get {return MovieTableViewCell.identifier}
        set {}
    }
    
    init(genreModel:Genre) {
        genretTitle = genreModel.name ?? ""
    }
    
    init(title:String) {
        genretTitle = title
    }

}
