//
//  GenericDetailsCollectionCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class GenericDetailsCollectionCellViewModel: BaseCellViewModel {
    
    // MARK: - variables
    var imageURL:String?
    var title:String = ""
    var subtitle:String = ""
    
    override var cellIdentifier: String{
        get {return GenericDetailsCollectionViewCell.identifier}
        set {}
    }
}
