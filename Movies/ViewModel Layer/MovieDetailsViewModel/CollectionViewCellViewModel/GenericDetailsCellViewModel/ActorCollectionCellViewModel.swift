//
//  ActorCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class ActorCollectionCellViewModel: GenericDetailsCollectionCellViewModel {
    
    // MARK: - variables
    private var actorModel:Actor
    
    // MARK: - intialization
    init(actor:Actor) {
        self.actorModel = actor
        super.init()
        configureData()
    }
    
    // MARK: - Data Configuration
    func configureData(){
        if let imagePath = actorModel.profilePath{
            imageURL = EnviromentManager.shared.imageBaseURL + imagePath
        }
        title = actorModel.name ?? "-"
        subtitle = actorModel.character ?? ""
    }
    
}
