//
//  MovieCollectionCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import UIKit

class MovieCollectionCellViewModel: GenericDetailsCollectionCellViewModel {
    
    // MARK: - variables
    private var movieModel:Movie
    var movieId:Int!
    
    // MARK: - intialization
    init(movie:Movie) {
        self.movieModel = movie
        super.init()
        configureData()
    }
    
    // MARK: - Data Configuration
    func configureData(){
        movieId = movieModel.id ?? 0
        if let imagePath = movieModel.posterPath{
            imageURL = EnviromentManager.shared.imageBaseURL + imagePath
        }
        title = movieModel.title ?? "-"
        subtitle = ""
    }
    
}
