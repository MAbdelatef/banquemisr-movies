//
//  MovieDetailsHeaderCellViewModel.swift
//  Movies
//
//  Created by Mohamed Abd el-latef on 6/9/20.
//  Copyright © 2020 Banque Misr. All rights reserved.
//

import Foundation
import RxSwift

class MovieDetailsHeaderViewModel: BaseHeaderViewModel {

    // MARK: - variables
    var title:String
    var buttonTitle:String?
    let buttonActionSubject = PublishSubject<Void>()
    
    // MARK: - intialization
    init(viewModels:[BaseCellViewModel], title:String = "", headerEnabled:Bool = false, buttonTitle:String? = nil) {
        self.title = title
        self.buttonTitle = buttonTitle
        super.init()
        self.headerEnabled = headerEnabled
        self.cellViewModels = viewModels
    }
    
    // MARK: - actions
    func handleButtonAction(){
        buttonActionSubject.onNext(())
    }
    
    override func headerHeight() -> CGFloat{
        return headerEnabled ? 80.0 : 0
    }
    
    override var headerIdentifier: String{
        get { MovieDetailsSectionHeaderView.identifier }
        set{}
    }

}
